<?php
include_once 'conf.php';
include_once 'admin-classes.php';

if (isset($_POST['submit'])) {

	$id = $_POST['id'];
	$name = $_POST['author'];
	$email = $_POST['email'];
	$title = $_POST['title'];
	$post = $_POST['msg'];
	$post_status = $_POST['status'];

	$obj = new ReviewCount();
	$edit = $obj->update($id, $name, $email, $title, $post, $post_status);

	header('Location: '. SITEURL . 'admin/?edit='.$id.'&statusedit=1');
}
?>