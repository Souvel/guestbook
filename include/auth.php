<?php
include_once '../include/conf.php';
include_once '../include/admin-classes.php';

$auth = new AdminSession();

if (isset($_POST['submit'])) {
	$login = $_POST['login'];
	$password = $_POST['password'];

	if (isset($login) && isset($password)) {
		if ($auth->auth($login, $password)) {
			header("Location: ".SITEURL."admin/");
		} else {
			header("Location: ".SITEURL."admin?auth=false&error=1");
		}
	} else {
		header("Location: ".SITEURL>"admin?auth=false&error=2");
	}
}

?>