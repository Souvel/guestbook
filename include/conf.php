<?php
define('SITEURL', '//myproj.test1.ru/GuestBook/');		// Адрес сайта
define('SITENAME', 'Guest Book');						// Имя сайта
define('LOGIN', 'admin');								// Логин в админ панель
define('PASS', 'admin');								// Пароль в админ панель
define('DBHOST', 'localhost');							// HOST базы данных
define('DBLOGIN', 'root');								// Логин базы данных
define('DBPASS', '');									// Пароль базы данных
define('DBBASE', 'guest_book');							// База данных
define('TABLENAME', 'guest_book');						// Таблица данных
define('ORDERBYPOST', 'DESC');							// Сортировка постов для вывода: ASC - от старых к новым; DESC - от новых к старым.
define('TIMEZONE', 'Europe/Minsk');						// Установка часового пояса //www.php.net/manual/ru/timezones.php
$postperpage = '10';									// Пагинация, кол-во записей на страницу
$sidebar = 'on';										// Включение/ отключение сайдбара, on - включен.


date_default_timezone_set(TIMEZONE);
$get_c = $_GET['c'];
$get_add = $_GET['add'];
$get_name = $_GET['name'];
$postedit = $_GET['edit'];
?>