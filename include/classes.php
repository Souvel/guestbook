<?php
class DBConnect {
	function __construct() {
		$connect_db = mysql_connect(DBHOST, DBLOGIN, DBPASS)
			or die ("DB Connection is not established: " . mysql_error());
		mysql_select_db(DBBASE, $connect_db)
			or die ("DB is not selected: " . mysql_error());
	}

	function __destruct() {
		mysql_close();
	}
}

class NewReview extends DBConnect {
	function __construct() {
		parent::__construct();
	}

	public function insert($name, $email, $title, $post, $date) {
		$name = trim($name);
		$name = htmlspecialchars($name, ENT_QUOTES);
		$name = mysql_real_escape_string($name);
		$email = trim($email);
		$email = htmlspecialchars($email, ENT_QUOTES);
		$email = mysql_real_escape_string($email);
		$title = trim($title);
		$title = htmlspecialchars($title, ENT_QUOTES);
		$title = mysql_real_escape_string($title);
		$post = trim($post);
		$post = htmlspecialchars($post, ENT_QUOTES);
		$post = mysql_real_escape_string($post);
		$query_post = "INSERT INTO ".TABLENAME." (name, email, title, post, date) VALUES ('{$name}', '{$email}', '{$title}', '{$post}', '{$date}')";
		$q = mysql_query($query_post)
			or die ("Your note is not added: " . mysql_error());
	}

}

class ViewReview extends DBConnect {
	function __construct() {
		parent::__construct();
	}

	public function select() {
		$query_post = "SELECT id, name, email, title, post, date FROM ".TABLENAME." WHERE post_status='3' ORDER BY date ".ORDERBYPOST;
		$q = mysql_query($query_post)
			or die ("Fatal error!".mysql_error());
		return $q;
	}

}