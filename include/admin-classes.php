<?php
include_once 'conf.php';

class DBConnect {
	function __construct() {
		$connect_db = mysql_connect(DBHOST, DBLOGIN, DBPASS)
			or die ("DB Connection is not established: " . mysql_error());
		mysql_select_db(DBBASE, $connect_db)
			or die ("DB is not selected: " . mysql_error());
	}

}

class AdminSession {

	public function IsAuth() {
		if (isset($_SESSION["is_auth"])) {
			return $_SESSION["is_auth"];
		} else return false;
	}

	public function Auth($login, $password) {
		if ($login == LOGIN && $password == PASS) {
			session_start();
			$_SESSION["is_auth"] = true;
			$_SESSION["login"] = LOGIN;
			return true;
		} else {
			$_SESSION["is_auth"] = false;
			return false;
		}
	}

	public function GetLogin() {
		if ($this->IsAuth()) {
			return $_SESSION["login"];
		}
	}

	public function Out() {
		$_SESSION = array();
		session_destroy();
		session_unset();
	}
}

class ReviewCount extends DBConnect {
	function __construct() {
		parent::__construct();
	}

	public function count_per_status($statusid) {
		if ($statusid == 0) {
			$query = "SELECT COUNT(*) FROM ".TABLENAME;
		} else {
			$query = "SELECT COUNT(*) FROM ".TABLENAME." WHERE post_status='{$statusid}'";
		}
		$q = mysql_query($query)
			or die ("Count Error!".mysql_error());
		while ($c = mysql_fetch_array($q, MYSQL_NUM)) {
			return $c[0];
		}
	}

	public function select($status) {
		if ($status == 0) {
			$query_post = "SELECT id, name, email, title, post, date FROM ".TABLENAME." ORDER BY date ".ORDERBYPOST;
		} else {
			$query_post = "SELECT id, name, email, title, post, date FROM ".TABLENAME." WHERE post_status='{$status}' ORDER BY date ".ORDERBYPOST;
		}
		$q = mysql_query($query_post)
			or die ("Fatal error!".mysql_error());
		return $q;
	}

	public function selecttoedit ($id) {
		$query_post = "SELECT name, email, title, post, date, post_status FROM ".TABLENAME." WHERE ID='{$id}'";
		$q = mysql_query($query_post)
			or die ("Post is not selected!".mysql_error());
		while ($post_array = mysql_fetch_array($q, MYSQL_NUM)) {
			$name = $post_array[0];
			$email = $post_array[1];
			$title = $post_array[2];
			$post = $post_array[3];
			$date = $post_array[4];
			$post_status = $post_array[5];
			$arraypost = array( 'name' => $name, 'email' => $email, 'title' => $title, 'post' => $post, 'date' => $date, 'status' => $post_status );
		return $arraypost;
		}
	}

	public function update ($id, $name, $email, $title, $post, $post_status) {
		$query_post = "UPDATE ".TABLENAME." SET name='{$name}', email='{$email}', title='{$title}', post='{$post}', post_status='{$post_status}' WHERE ID='{$id}'";
		$q = mysql_query($query_post)
			or die ("Your post is not Updated!".mysql_error());
	}
}