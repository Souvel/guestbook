
-- База данных:
-- ########################################################################################
-- #   ID   ###    Name    ###    E-mail    ###    Post    ###    Date    ###   Status    #
-- ########################################################################################
-- #        ###            ###              ###            ###            ###             #
-- #        ###            ###              ###            ###            ###             #

-- Описание полей:
-- 1. ID;
-- 2. name - Имя автора отзыва (60 символов);
-- 3. email - E-mail автора (100 символов);
-- 4. post - Отзыв (5000 символов);
-- 5. date - Дата отзыва;
-- 6. post_status - Статуст отзыва ( 1 - pending (по умолчанию), 2 - cancelled, 3 - approved, 4 - deleted).


DROP TABLE IF EXISTS `guest_book`;
CREATE TABLE `guest_book` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `post` varchar(5000) NOT NULL DEFAULT '',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;