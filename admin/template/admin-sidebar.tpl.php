<?php
include_once '../include/admin-classes.php';

$count = new ReviewCount();
$all = $count->count_per_status(0);
$pending = $count->count_per_status(1);
$cancelled = $count->count_per_status(2);
$approved = $count->count_per_status(3);
$deleted = $count->count_per_status(4);
?>
<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li><a href="<?php echo SITEURL; ?>admin/?poststatus=0"><span class="badge pull-right"><?php echo $all; ?></span>All</a></li>
        <li><a href="<?php echo SITEURL; ?>admin/?poststatus=1"><span class="badge pull-right"><?php echo $pending; ?></span>Pending</a></li>
        <li><a href="<?php echo SITEURL; ?>admin/?poststatus=2"><span class="badge pull-right"><?php echo $cancelled; ?></span>Cancelled</a></li>
        <li><a href="<?php echo SITEURL; ?>admin/?poststatus=3"><span class="badge pull-right"><?php echo $approved; ?></span>Approved</a></li>
        <li><a href="<?php echo SITEURL; ?>admin/?poststatus=4"><span class="badge pull-right"><?php echo $deleted; ?></span>Deleted</a></li>
    </ul>
    <ul class="nav nav-sidebar">
        <li><a href="<?php echo SITEURL; ?>admin/?edit=0">Edit</a></li>
    </ul>
</div>