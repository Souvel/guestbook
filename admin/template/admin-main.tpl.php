<?php
include_once '../include/admin-classes.php';

$post_status = $_GET['poststatus'];
$post_st = 0;
if (isset($post_status)) {
    $post_st = $post_status;
}
$count = new ReviewCount();
$cc =  $count->count_per_status($post_st);
$sel = $count->select($post_st);

?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2 class="sub-header">Posts - <?php echo $cc; ?></h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="5%"><div class="glyphicon glyphicon-cog"></div></th>
                    <th width="10%">Author</th>
                    <th width="20%">Email</th>
                    <th width="20%">Title</th>
                    <th>Message</th>
                </tr>
            </thead>
            <tbody>
            <?php
            while ($c = mysql_fetch_array($sel, MYSQL_NUM)) {
                $id = $c[0];
                $name = $c[1];
                $email = $c[2];
                $title = $c[3];
                $post = $c[4];

                printf("<tr><td><a href=\"".SITEURL."admin/?edit=$id\">$id</a></td><td>$name</td><td>$email</td><td>$title</td><td>$post</td></tr>");
            }

            ?>
            </tbody>
        </table>
    </div>
</div>