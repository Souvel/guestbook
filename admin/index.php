<?php
session_start();
include_once '../include/admin-classes.php';
include_once '../include/conf.php';

$auth = new AdminSession();

if (isset($_GET['logout'])) {
	if ($_GET['logout'] == 1) {
		$auth->Out();
		header("Location: ".SITEURL."admin?logout=0");
	}
}

if ($auth->IsAuth()) {

	include_once '../template/admin-header.tpl.php';
	include_once '../template/admin-sidebar.tpl.php';

	if (isset($postedit)) {
		include_once '../template/admin-edit.tpl.php';
	} else {
		include_once '../template/admin-main.tpl.php';
	}

	include_once '../template/admin-footer.tpl.php';

} else {
	include_once '../template/auth.tpl.php';
}
?>