<?php
$auth = $_GET['auth'];
$errorauth = $_GET['error'];
?> 
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The administrative center of the guest book</title>
    <link rel="icon" href="<?php echo SITEURL ?>favicon.ico" sizes="16x16" type="image/x-icon">
    <link href="<?php echo SITEURL; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITEURL; ?>css/style-admin.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
    <?php if (isset($auth) && isset($errorauth)) echo "<div class=\"form-group has-error\">"; ?>
    <form action="<?php echo SITEURL; ?>include/auth.php" class="form-signin" role="form" method="POST">
        <h2 class="form-signin-heading">Login Form</h2>
            <input type="name" name="login" class="form-control" value="<?php echo LOGIN; ?>" required autofocus>
            <input type="password" name="password" class="form-control" value="<?php echo PASS; ?>" required>
        <center><h4><small>Just click on the button <?php echo SITEURL; ?>include/auth.php</small></h4></center>
        <br>
        <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    </form>
    <?php if (isset($auth) && isset($errorauth)) echo "</div>"; ?>
    <center><h4><small><a href="<?php echo SITEURL; ?>"><div class="glyphicon glyphicon-home"></div> Home Page</a></small></h4></center>

</div> <!-- /container -->
</body>
</html>