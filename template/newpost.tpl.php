<div class="col-sm-8 blog-main">

	<form action="include/newpost.php" method="post" id="new_post" novalodate="novalidate">
		<div class="row">
			<div class="col-xs-6">
				<div class="input-group">
					<span class="input-group-addon"><div class="glyphicon glyphicon-user"></div></span>
					<input type="text" id="author" name="author" class="form-control" placeholder="Enter the name" autofocus>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="input-group">
					<span class="input-group-addon"><div class="glyphicon glyphicon-envelope"></div></span>
					<input type="text" id="email" name="email" class="form-control" placeholder="Enter the email">
				</div>
			</div>
		</div><br>
		<div class="input-group">
			<span class="input-group-addon"><div class="glyphicon glyphicon-pencil"></div></span>
			<input type="text" id="title" name="title" class="form-control" placeholder="Enter the subject">
		</div><br>
		<div class="input-group">
			<span class="input-group-addon"><div class="glyphicon glyphicon-book"></div></span>
			<textarea class="form-control" id="msg" name="msg" rows="5" placeholder="Your review! (2000 character limit)" style="resize: none;"></textarea>
		</div><br>
		<button name="submit" type="submit" class="btn btn-primary btn-lg btn-block"><div class="glyphicon glyphicon-ok"></div> Leave Review</button>
	</form><br>

</div><!-- /.blog-main -->