<?php
include_once '../include/admin-classes.php';

$post_status_view = array( 0 => "All", 1 => "Pending", 2 => "Cancelled", 3 => "Approved", 4 => "Deleted" );
if(is_numeric($postedit)) {
    $seledit = new ReviewCount();
    $query = $seledit->selecttoedit($postedit);
    $status = $query['status'];
}

?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h2 class="sub-header"><?php echo $query['title']; ?> | <b><?php echo $post_status_view[$status]; ?></b></h2>
    <div class="table-responsive">
        <form action="../include/admin-edit.php" method="post" id="new_post" novalodate="novalidate">
        <div style="display: none;">
            <input type="hidden" name="id" value="<?php echo $postedit; ?>">
        </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon"><div class="glyphicon glyphicon-user"></div></span>
                        <input type="text" id="author" name="author" class="form-control" placeholder="Enter the name" value="<?php echo $query['name']; ?>">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon"><div class="glyphicon glyphicon-envelope"></div></span>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Enter the email"value="<?php echo $query['email']; ?>">
                    </div>
                </div>
            </div><br>
            <div class="input-group">
                <span class="input-group-addon"><div class="glyphicon glyphicon-pencil"></div></span>
                <input type="text" id="title" name="title" class="form-control" placeholder="Enter the subject" value="<?php echo $query['title']; ?>">
            </div><br>
            <div class="input-group">
                <span class="input-group-addon"><div class="glyphicon glyphicon-book"></div></span>
                <textarea class="form-control" id="msg" name="msg" rows="5" placeholder="Your review! (2000 character limit)" style="resize: none;"><?php echo $query['post']; ?></textarea>
            </div><br>
            <div class="input-group">
                <span class="input-group-addon"><div class="glyphicon glyphicon-calendar"></div></span>
                <input type="text" id="disabledInput" name="date" class="form-control" placeholder="Enter the subject" value="<?php echo $query['date']; ?>" disabled>
            </div><br>
            <div class="input-group">
                <span class="input-group-addon"><div class="glyphicon glyphicon-wrench"></div></span>
                <select class="form-control" autofocus name="status" required><option value="1" disabled selected="selected">Pending</option><option value="2">Cancelled</option><option value="3">Approved</option><option value="4">Deleted</option></select>
            </div><br>
            <button name="submit" type="submit" class="btn btn-primary btn-lg btn-block">Edit Review</button>
        </form><br>
    </div>
</div>