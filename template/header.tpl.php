<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php if ($get_c == 'new') {echo "New Review";}else{echo "GuestBook";} ?></title>
	<link rel="icon" href="<?php echo SITEURL ?>favicon.ico" sizes="16x16" type="image/x-icon">
	<link href="<?php echo SITEURL; ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo SITEURL; ?>css/main.css" rel="stylesheet">
</head>
<body>
	<div class="blog-masthead">
		<div class="container">
			<nav class="blog-nav">
				<a class="blog-nav-item <?php if ($get_c != 'new') echo "active"; ?>" href="<?php echo SITEURL; ?>">Home Page</a>
				<a class="blog-nav-item <?php if ($get_c == 'new') echo "active"; ?>" href="<?php echo SITEURL; ?>?c=new">New Review</a>
				<a class="blog-nav-item" href="<?php echo SITEURL; ?>admin">Admin Panel</a>
			</nav>
		</div>
	</div>
	<div class="container">

		<div class="blog-header">
			<h1 class="blog-title">Guest Book</h1>
			<p class="lead blog-description">Leave your Review!</p>
		</div>