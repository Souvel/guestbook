<?php
include_once '../include/admin.php';
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel</title>
    <link rel="icon" href="<?php echo SITEURL ?>favicon.ico" sizes="16x16" type="image/x-icon">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style-admin1.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo SITEURL; ?>"><div class="glyphicon glyphicon-home"></div> <?php echo SITENAME; ?></a>
            <a class="navbar-brand" href="<?php echo SITEURL."admin/"; ?>"><div class="glyphicon glyphicon-cog"></div> Admin Panel</a>
            <a class="navbar-brand" href="<?php echo SITEURL."admin?logout=1"; ?>"><div class="glyphicon glyphicon-upload"></div> LogOut</a>
        </div>
    </div>
</div>
<div class="container-fluid">
<div class="row">